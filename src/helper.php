<?php

//  Load Libraries
use \Messenger\Core\Manager;


/**
 * Get the Default Platform
 *
 * @param null $platform
 * @return Manager
 */
function defaultPlatformManager( $platform = null )
{
    //  Global
    global $def_platform_manager;

    //  Check
    if( $platform )
    {
        //  Assign
        $def_platform_manager = $platform;
    }

    //  Check
    if( !$def_platform_manager )    $def_platform_manager = 'facebook';

    //  Return
    return $def_platform_manager;
}

/**
 * Get the Manager
 *
 * @param array ...$args
 * @return Manager
 */
function mManager( ...$args )
{
    //  Global
    global $mManagers;

    //  Check
    if( !$mManagers )   $mManagers = array();

    //  Platform
    $platform = ( sizeof( func_get_args() ) > 0 ? func_get_arg(0 ) : null );

    //  Fix
    if( !$platform )    $platform = defaultPlatformManager();

    //  Check
    if( !isset( $mManagers[$platform] ) )   $mManagers[$platform] = new Manager( ...$args );

    //  Return
    return $mManagers[$platform];
}

/**
 * Get All Managers
 *
 * @return array
 */
function mManagers()
{
    //  Global
    global $mManagers;

    //  Check
    if( !$mManagers )   $mManagers = array();

    //  Return
    return $mManagers;
}