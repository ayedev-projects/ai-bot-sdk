<?php namespace AI\Provider\ApiAI\Factory;

use AI\Impl\GeneratorFactory;
use Messenger\IFace\EventInterface;
use Messenger\Provider\Facebook\Model\Attachment\Image;
use Messenger\Provider\Facebook\Model\Attachment\Template\Generic;
use Messenger\Provider\Facebook\Model\Attachment\Template\Generic\Element;
use Messenger\Provider\Facebook\Model\Button\Postback;
use Messenger\Provider\Facebook\Model\Button\Share;
use Messenger\Provider\Facebook\Model\Button\WebUrl;
use Messenger\Provider\Facebook\Model\DefaultAction;
use Messenger\Provider\Facebook\Model\Message;
use Messenger\Provider\Facebook\Model\QuickReply;

class FacebookAPIAIFactory extends GeneratorFactory
{
    /**
     * @inheritdoc
     */
    protected function parseMessage( EventInterface $event, array $messageData )
    {
        //  Message
        $message = null;

        //  Try
        try
        {
            //  Switch
            switch( $messageData['type'] )
            {
                //  Simple Message
                case 0:

                    //  Create Message
                    $message = new Message( $this->fill( $event, $messageData['speech'] ) );
                    break;

                //  Card Message
                case 1:

                    //  Create Element
                    $element = new Element(
                        $this->fill( $event, $messageData['title'] ),
                        ( isset( $messageData['subtitle'] ) ? $this->fill( $event, $messageData['subtitle'] ) : null ),
                        ( isset( $messageData['imageUrl'] ) ? $this->fill( $event, $messageData['imageUrl'] ) : null )
                    );

                    //  Check
                    if( isset( $messageData['buttons'] ) )
                    {
                        //  Loop Each Buttons
                        foreach( $messageData['buttons'] as $buttonDef )
                        {
                            //  Type
                            $type = ( isset( $buttonDef['postback'] ) ? 'postback' : 'url' );

                            //  Type Data
                            $typeData = $buttonDef[$type];

                            //  Check Overrides
                            if( substr( $typeData, 0, 4 ) == 'url:' )
                            {
                                //  Change Type
                                $type = 'url';

                                //  Trim Data
                                $typeData = substr( $typeData, 4 );
                            }
                            else if( substr( $typeData, 0, 9 ) == 'postback:' )
                            {
                                //  Change Type
                                $type = 'postback';

                                //  Trim Data
                                $typeData = substr( $typeData, 9 );
                            }
                            else if( substr( $typeData, 0, 6 ) == 'share:' )
                            {
                                //  Change Type
                                $type = 'share';

                                //  Trim Data
                                $typeData = substr( $typeData, 6 );
                            }

                            //  Text
                            $buttonText = $this->fill( $event, $buttonDef['text'] );

                            //  Run Filler
                            $typeData = $this->fill( $event, $typeData );

                            //  Button
                            $button = null;

                            //  Switch
                            switch( $type )
                            {
                                //  Url
                                case 'url':

                                    //  Create Button
                                    $button = new WebUrl( $buttonText, $typeData );
                                    break;

                                //  Postback
                                case 'postback':

                                    //  Create Button
                                    $button = new Postback( $buttonText, strtoupper( $typeData ) );
                                    break;

                                //  Share
                                case 'share':

                                    //  Create Button
                                    $button = new Share();
                                    break;
                            }

                            //  Check
                            if( $button )   $element->addButton( $button );
                        }

                        //  Get First Button
                        $firstButton = $element->getFirstButton();

                        //  Check
                        if( $firstButton && $firstButton instanceof WebUrl )  $element->setDefaultAction( new DefaultAction( $firstButton->getUrl() ) );
                    }

                    //  Create Message
                    $message = new Generic( array( $element ) );
                    break;

                //  Quick Reply
                case 2:

                    //  Create Message
                    $message = new Message( $this->fill( $event, $messageData['title'] ) );

                    //  Loop Each Quick Replies
                    foreach( $messageData['replies'] as $replyText )
                    {
                        //  Button Payload
                        $replyPayload = null;

                        //  Check
                        if( preg_match( '/^\[(?<payload>[a-zA-Z0-9\_]+)\]/i', $replyText, $match ) )
                        {
                            //  Set Payload Text
                            $replyPayload = strtoupper( $match['payload'] );

                            //  Update the Reply Text
                            $replyText = trim( str_ireplace( $match[0], '', $replyText ) );
                        }

                        //  Check
                        if( !$replyPayload )
                        {
                            //  Create from Text
                            $replyPayload = strtoupper( 'quickreply_' . str_ireplace( ' ', '_', $replyText ) );
                        }

                        //  Append
                        $message->addQuickReply( new QuickReply( $replyText, $replyPayload ) );
                    }
                    break;

                //  Image Message
                case 3:

                    //  Create Message
                    $message = new Image( $this->fill( $event, $messageData['imageUrl'] ) );
                    break;
            }
        } catch( \Exception $e ) {}

        //  Return
        return $message;
    }
}