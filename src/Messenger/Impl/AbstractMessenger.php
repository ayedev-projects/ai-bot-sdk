<?php namespace Messenger\Impl;

use Messenger\Core\MessengerResponse;
use Messenger\IFace\MessengerInterface;

abstract class AbstractMessenger extends AbstractSkeleton implements MessengerInterface
{
    /**
     * Send Message
     *
     * @return mixed
     */
    public static function send()
    {
        //  Return
        return self::call( 'sendMessage', func_get_args() );
    }
}