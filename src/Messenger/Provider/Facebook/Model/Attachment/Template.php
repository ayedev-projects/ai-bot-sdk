<?php namespace Messenger\Provider\Facebook\Model\Attachment;

use Messenger\Impl\AbstractMessage;

class Template extends AbstractMessage implements \JsonSerializable
{
    //  Types
    const TYPE_GENERIC = 'generic';
    const TYPE_BUTTON = 'button';
    const TYPE_LIST = 'list';

    /** @var array $_fillable */
    protected $_fillable = array( 'type', 'payload' );


    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[TEMPLATE]';
    }
}