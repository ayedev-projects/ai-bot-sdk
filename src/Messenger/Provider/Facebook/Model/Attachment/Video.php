<?php namespace Messenger\Provider\Facebook\Model\Attachment;

use Messenger\Provider\Facebook\Model\Attachment;

class Video extends File
{
    /**
     * Video constructor.
     *
     * @param $filePath
     */
    public function __construct( $filePath )
    {
        //  Run Parent
        parent::__construct( $filePath, Attachment::TYPE_VIDEO );
    }
}