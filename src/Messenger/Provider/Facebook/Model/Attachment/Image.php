<?php namespace Messenger\Provider\Facebook\Model\Attachment;

use Messenger\Provider\Facebook\Model\Attachment;

class Image extends File
{
    /** @var array $allowedExtensions */
    protected $allowedExtensions = ['jpg', 'gif', 'png'];


    /**
     * Image constructor.
     *
     * @param $filePath
     */
    public function __construct( $filePath )
    {
        //  Run Constructor
        parent::__construct( $filePath, Attachment::TYPE_IMAGE );
    }
}