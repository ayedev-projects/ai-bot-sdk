<?php namespace Messenger\Provider\Facebook\Model\Attachment;

use Messenger\Provider\Facebook\Model\Attachment;

class PayloadAttachment extends Attachment
{
    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[PAYLOAD_ATTACHMENT]';
    }

    /**
     * @inheritdoc
     */
    public function toArray()
    {
        //  Return
        return [
            'attachment' => parent::toArray()
        ];
    }
}