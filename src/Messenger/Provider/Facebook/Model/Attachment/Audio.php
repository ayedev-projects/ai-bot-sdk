<?php namespace Messenger\Provider\Facebook\Model\Attachment;

use Messenger\Provider\Facebook\Model\Attachment;

class Audio extends File
{
    /**
     * Audio constructor.
     *
     * @param $filePath
     */
    public function __construct( $filePath )
    {
        //  Run Parent
        parent::__construct( $filePath, Attachment::TYPE_AUDIO );
    }
}