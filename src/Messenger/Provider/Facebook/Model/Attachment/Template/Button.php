<?php namespace Messenger\Provider\Facebook\Model\Attachment\Template;

use Messenger\Provider\Facebook\Model\Attachment\Template;
use Messenger\Provider\Facebook\Model\Button as ButtonModel;

class Button extends Template
{
    /** @var array $_fillable */
    protected $_fillable = array( 'template_type', 'text', 'buttons' );


    /**
     * Constructor
     *
     * @param null|string $text
     * @param ButtonModel[] $buttons
     */
    public function __construct( $text, array $buttons = [] )
    {
        //  Set Text
        $this->setText( $text );

        //  Set Buttons
        $this->setButtons( $buttons );

        //  Set Template Type
        $this->setTemplateType( Template::TYPE_BUTTON );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return 'Button: ' . $this->getText();
    }
}