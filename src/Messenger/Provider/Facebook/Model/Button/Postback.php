<?php namespace Messenger\Provider\Facebook\Model\Button;

use Messenger\Provider\Facebook\Model\Button;

class Postback extends Button
{
    /** @var array $_fillable */
    protected $_fillable = array( 'type', 'title', 'payload' );


    /**
     * @param string $title
     * @param string $payload
     */
    public function __construct($title, $payload)
    {
        //  Set Type
        $this->setType( Button::TYPE_POSTBACK );

        //  Validate Title Size
        self::validateTitleSize( $title );

        //  Set Title
        $this->setTitle( $title );

        //  Set Payload
        $this->setPayload( $payload );
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return $this->getPayload();
    }
}