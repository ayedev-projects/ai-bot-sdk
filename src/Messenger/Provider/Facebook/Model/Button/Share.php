<?php namespace Messenger\Provider\Facebook\Model\Button;

use Messenger\Provider\Facebook\Model\Button;

class Share extends Button
{
    /**
     * Share constructor.
     */
    public function __construct()
    {
        //  Set Type
        $this->setType( Button::TYPE_SHARE );
    }
}