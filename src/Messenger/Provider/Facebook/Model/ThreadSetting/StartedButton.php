<?php namespace Messenger\Provider\Facebook\Model\ThreadSetting;

use Messenger\Provider\Facebook\Model\ThreadSetting;

class StartedButton implements ThreadSetting, \JsonSerializable
{
    /**
     * @var string
     */
    private $payload;


    /**
     * StartedButton constructor.
     *
     * @param $payload
     */
    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'payload' => $this->payload,
        ];
    }
}