<?php namespace Messenger\Provider\Facebook\Model\QuickReply;

use Messenger\Provider\Facebook\Model\QuickReply;

class Location extends QuickReply
{
    /** @var string $_contentType */
    protected $_contentType = 'location';


    /**
     * Location constructor.
     */
    public function __construct()
    {
        //  Change Content Type
        $this->setContentType( 'location' );
    }
}