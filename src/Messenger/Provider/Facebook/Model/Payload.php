<?php namespace Messenger\Provider\Facebook\Model;

use Messenger\Traits\MessageTrait;

class Payload implements \JsonSerializable
{
    use MessageTrait;

    /** @var array $_fillable */
    protected $_fillable = true;

    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[RAW_PAYLOAD]';
    }
}