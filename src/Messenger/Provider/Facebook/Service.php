<?php namespace Messenger\Provider\Facebook;

use Messenger\Impl\AbstractMessage;
use Messenger\Core\ApiCall;
use Messenger\Impl\AbstractMessenger;
use Messenger\Provider\Facebook\Model\Attachment\PayloadAttachment;
use Messenger\Provider\Facebook\Model\Attachment\Template;
use Messenger\Provider\Facebook\Model\Message;
use Messenger\Provider\Facebook\Model\ThreadSetting;
use Messenger\Provider\Facebook\Model\ThreadSetting\GreetingText;
use Messenger\Provider\Facebook\Model\ThreadSetting\StartedButton;
use Messenger\Provider\Facebook\Model\UserProfile;

class Service extends AbstractMessenger
{
    /**
     * @inheritdoc
     */
    public function sendMessage( $recipient, $message, $notificationType = null )
    {
        //  Check
        if( !$notificationType )    $notificationType = Core\NotificationType::REGULAR;

        //  Create Message
        $message = $this->createMessage( $message );

        //  Generate Options for Message
        $options = Request::make( $recipient, $message, $notificationType );

        //  Run Graph
        $responseData = ApiCall::instance()->send( 'POST', '/me/messages', null, [], [], $options )->getResponseDecoded();

        //  Return
        return new Response( $responseData['recipient_id'], $responseData['message_id'], ( isset( $responseData['attachment_id'] ) ? $responseData['attachment_id'] : null ) );
    }

    /**
     * @inheritdoc
     */
    public function getUserProfile( $userId, $fields = array() )
    {
        //  Check
        if( !$fields )
        {
            //  Assign
            $fields = [
                UserProfile::FIRST_NAME,
                UserProfile::LAST_NAME,
                UserProfile::PROFILE_PIC,
                UserProfile::LOCALE,
                UserProfile::TIMEZONE,
                UserProfile::GENDER,
                UserProfile::PAYMENT_ENABLED,
            ];
        }

        //  Query
        $query = [
            'fields' => implode(',', $fields)
        ];

        //  Get Response
        $response = ApiCall::instance()->get( sprintf( '/%s', $userId ), $query )->getResponseDecoded();

        //  Return
        return UserProfile::create( $response );
    }

    /**
     * Subscribe the app to the page
     *
     * @return bool
     */
    public function subscribe()
    {
        //  Get Response
        $response = ApiCall::instance()->post( '/me/subscribed_apps' )->getResponseDecoded();

        //  Return
        return $response['success'];
    }

    /**
     * Set Greeting Text
     *
     * @param $text
     * @return $this
     */
    public function setGreetingText( $text )
    {
        //  Greeting Text
        $greeting = new GreetingText( $text );

        //  Build Setting
        $setting = $this->buildSetting( ThreadSetting::TYPE_GREETING, null, $greeting );

        //  Post Settings
        $this->postThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Delete Greeting Text
     *
     * @return $this
     */
    public function deleteGreetingText()
    {
        //  Build Setting
        $setting = $this->buildSetting( ThreadSetting::TYPE_GREETING );

        //  Delete Settings
        $this->deleteThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Set Started Button
     *
     * @param string $payload
     * @return $this
     */
    public function setStartedButton( $payload )
    {
        //  Started Button
        $startedButton = new StartedButton( $payload );

        //  Build Setting
        $setting = $this->buildSetting(
            ThreadSetting::TYPE_CALL_TO_ACTIONS,
            ThreadSetting::NEW_THREAD,
            [$startedButton]
        );

        //  Post Settings
        $this->postThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Delete Started Button
     *
     * @return $this
     */
    public function deleteStartedButton()
    {
        //  Build Setting
        $setting = $this->buildSetting(
            ThreadSetting::TYPE_CALL_TO_ACTIONS,
            ThreadSetting::NEW_THREAD
        );

        //  Delete Settings
        $this->deleteThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Set Persistent Menu
     *
     * @param array $menuButtons
     * @return $this
     */
    public function setPersistentMenu( array $menuButtons )
    {
        //  Check
        if( count( $menuButtons ) > 5 ) throw new \InvalidArgumentException( 'You should not set more than 5 menu items.' );

        //  Build Setting
        $setting = $this->buildSetting(
            ThreadSetting::TYPE_CALL_TO_ACTIONS,
            ThreadSetting::EXISTING_THREAD,
            $menuButtons
        );

        //  Post Settings
        $this->postThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Delete Persistent Menu
     *
     * @return $this
     */
    public function deletePersistentMenu()
    {
        //  Build Setting
        $setting = $this->buildSetting(
            ThreadSetting::TYPE_CALL_TO_ACTIONS,
            ThreadSetting::EXISTING_THREAD
        );

        //  Delete Setting
        $this->deleteThreadSettings( $setting );

        //  Return
        return $this;
    }

    /**
     * Post Thread Settings
     *
     * @param array $setting
     * @return array
     */
    private function postThreadSettings(array $setting)
    {
        //  Return
        return ApiCall::instance()->post( '/me/thread_settings', $setting )->getResponseDecoded();
    }

    /**
     * Delete Thread Settings
     *
     * @param array $setting
     * @return array
     */
    private function deleteThreadSettings(array $setting)
    {
        //  Return
        return ApiCall::instance()->delete( '/me/thread_settings', $setting )->getResponseDecoded();
    }

    /**
     * Build Settings
     *
     * @param string $type
     * @param null|string $threadState
     * @param mixed $value
     *
     * @return array
     */
    private function buildSetting($type, $threadState = null, $value = null)
    {
        //  Base Settings
        $setting = [
            'setting_type' => $type,
        ];

        //  Check State
        if( !empty( $threadState ) )    $setting['thread_state'] = $threadState;

        //  Check Value
        if( !empty( $value ) )  $setting[$type] = $value;

        //  Return
        return $setting;
    }

    /**
     * Create Message
     *
     * @param string|AbstractMessage $message
     * @return AbstractMessage
     */
    private function createMessage( $message )
    {
        //  Check for Template
        if( $message instanceof Template )
        {
            //  Create Message
            $message = new PayloadAttachment( PayloadAttachment::TYPE_TEMPLATE, $message );
        }

        //  Check for String or Attachment
        if( is_string( $message ) )
        {
            //  Create Message
            return new Message( $message );
        }

        //  Check for Message
        if( $message instanceof AbstractMessage )   return $message;

        //  Throw Exception
        throw new \InvalidArgumentException( 'Message should be a string, Message, Attachment or Template' );
    }
}