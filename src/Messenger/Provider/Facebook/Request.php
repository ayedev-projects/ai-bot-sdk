<?php namespace Messenger\Provider\Facebook;

use GuzzleHttp\RequestOptions;
use Messenger\Core\ApiCall;
use Messenger\IFace\MessengerRequestInterface;
use Messenger\Impl\AbstractMessage;
use Messenger\Provider\Facebook\Core\NotificationType;

class Request implements MessengerRequestInterface
{
    /** @var array $_request */
    private $_request = array();


    /**
     * Create for Message
     *
     * @param string $recipientOrPhone
     * @param AbstractMessage $message
     * @param string $notificationType
     */
    public function __construct( $recipientOrPhone, AbstractMessage $message, $notificationType = NotificationType::REGULAR )
    {
        //  Options
        $options = [];

        //  Data
        $data = [
            'recipient' => $this->createRecipientField( $recipientOrPhone ),
            'message' => $message,
            'notification_type' => $notificationType,
        ];

        //  Check has Upload
        if( $message->hasFileToUpload() )
        {
            //  Create a multipart request
            $options[RequestOptions::MULTIPART] = [
                [
                    'name' => 'recipient',
                    'contents' => json_encode($data['recipient']),
                ],
                [
                    'name' => 'message',
                    'contents' => json_encode($data['message']),
                ],
                [
                    'name' => 'notification_type',
                    'contents' => $data['notification_type'],
                ],
                [
                    'name' => 'filedata',
                    'contents' => $message->getFileStream(),
                ],
            ];

            // Update timeout if we upload a file
            $options['timeout'] = ApiCall::DEFAULT_FILE_UPLOAD_TIMEOUT;
        }
        else
        {
            //  Set JSON Data
            $options[RequestOptions::JSON] = $data;
        }

        //  Store
        $this->_request = $options;
    }

    /**
     * Get Request
     *
     * @return array
     */
    public function getRequest()
    {
        //  Return
        return $this->_request;
    }

    /**
     * Create for Recipient
     *
     * @param $recipientOrPhone
     * @return array
     */
    public function createRecipientField( $recipientOrPhone )
    {
        //  Field Name
        $recipientFieldName = ( strpos( $recipientOrPhone, '+' ) === 0 ? 'phone_number' : 'id' );

        //  Return
        return [$recipientFieldName => $recipientOrPhone];
    }


    /**
     * Get
     * @param $recipientOrPhone
     * @param AbstractMessage $message
     * @param string $notificationType
     * @return mixed
     */
    public static function make( $recipientOrPhone, AbstractMessage $message, $notificationType = NotificationType::REGULAR )
    {
        //  Create
        $ins = new static( $recipientOrPhone, $message, $notificationType );

        //  Return
        return $ins->getRequest();
    }
}