<?php namespace Messenger\Exception;

class UnknownMessageException extends MessengerException {}