<?php namespace Messenger\IFace;

use Messenger\IMpl\AbstractMessage;
use Messenger\Exception\ApiException;

interface MessengerInterface
{
    /**
     * Send Message
     *
     * @param string $recipient
     * @param string|AbstractMessage $message
     * @param string $notificationType
     *
     * @return mixed
     *
     * @throws ApiException
     */
    public function sendMessage( $recipient, $message, $notificationType = null );

    /**
     * Get User Profile
     *
     * @param string $userId
     * @param array $fields
     *
     * @return mixed
     */
    public function getUserProfile( $userId, $fields = array() );
}