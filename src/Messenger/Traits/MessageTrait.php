<?php namespace Messenger\Traits;

trait MessageTrait
{
    use KeyValuePairsTrait, PlainMessageTrait;
}