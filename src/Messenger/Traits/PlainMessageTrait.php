<?php namespace Messenger\Traits;

trait PlainMessageTrait
{
    /**
     * Convert to Plain Text
     *
     * @return string
     */
    abstract public function toString();
}